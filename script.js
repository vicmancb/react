/**
 * Created by vicman on 5/2/16.
 */

var Pokemon = React.createClass({

    getInitialState: function () {
      return {power : 10};
    },

    handleClick : function () {
        this.setState(function (state) {
            return {power: state.power + 1};
        });
    },

    render : function () {
        var name = this.props.name;
        var picture = this.props.picture;
        var power = this.state.power;
        return(
            <div className="content" onClick={this.handleClick}>
                <div className="image">
                    <img src={picture}/>
                </div>
                <div className="name">{name}</div>
                <div className="name">Poder: {power}</div>
            </div>);
    }
});

ReactDOM.render(
    <Pokemon
        picture="http://vignette3.wikia.nocookie.net/pokemon/images/1/16/025Pikachu_OS_anime_10.png/revision/20150102074354"
        name="Charmander"
    />,
    document.getElementById('pokemon')
);